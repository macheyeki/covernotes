SELECT 
mcn.insurer_id,
i.name insurer,
cat.category_name category,
mcn.class_id,
cl.class,
mcn.covernotetype_id covertype_id,
cnt.type covernotetype,
concat(mcn.insured_fname,' ',
mcn.insured_mname,' ',
mcn.insured_lname) as i_name,

mcn.sticker_no, 
mcn.icn_no icn,
mcn.email email,
mcn.mobile,
mcn.address pbox,
mcn.make,
mcn.reg_no,
mcn.engine_no,
mcn.chasis_no,
mcn.bodytype body,
mcn.cc,
mcn.yom,
mcn.capacity,
mcn.value,
pr.basic_premium bp,
pr.ebb,
pr.unexperience,
pr.underage,
pr.net_premium np,
pr.vat,
100* pr.brokerage_rate br_rate,
pr.brokerage br,
mcn.i_date,
mcn.e_date
FROM motor_covernotes as mcn
LEFT JOIN premium as pr ON mcn.id=pr.cover_id
LEFT JOIN insurers as i ON mcn.insurer_id = i.id
LEFT JOIN classes as cl ON mcn.class_id = cl.id
LEFT JOIN motors_category as cat  ON mcn.category_id = cat.id
LEFT JOIN cover_notes_types as cnt ON mcn.covernotetype_id = cnt.id;