$(document).ready(function(){


  $(".submenu > a").click(function(e) {
    e.preventDefault();
    var $li = $(this).parent("li");
    var $ul = $(this).next("ul");

    if($li.hasClass("open")) {
      $ul.slideUp(350);
      $li.removeClass("open");
    } else {
      $(".nav > li > ul").slideUp(350);
      $(".nav > li").removeClass("open");
      $ul.slideDown(350);
      $li.addClass("open");
    }
  });
  
});


// admin confirmation
$(document).ready(function() 
{ 
  $( "input[type=checkbox]" ).on( "click", function  () 
  {
  var that=$(this);
  if ( that.is(':checked') ) 
  {
    console.log(that.val()+" is checked");
    var status=that.val()+'|checked';
    $.ajax({
      url:"classenable",
      type:'POST',  
      data:'ajax=ajax&&status='+status,
      success:function(data) {
      console.log(status);
      }
    });
    }
    else
    {
    console.log(that.val()+" is unchecked");
    var status=that.val()+'|unchecked';
    $.ajax({
      url:"classenable",
      type:'POST',  
      data:'ajax=ajax&&status='+status,
      success:function(data) {
       console.log(status);
      }
    });
    }
  
} );
});


// motor vehicles calculation
// insurance
$(document).ready(function() 
{

$('#value').hide();
$('#bp').hide();
$('#np').hide();

$('#insurer,#cat,#ctype,#period,#sumedvalue,#capacity,#s_ebb,#s_unex,#s_uage').change(function()
  {

      var s_ebb = ($("#s_ebb").is(":checked")) ? "1" : "0";
      var s_unex = ($("#s_unex").is(":checked")) ? "1" : "0";
      var s_uage = ($("#s_uage").is(":checked")) ? "1" : "0"; 
      var value = $("#sumedvalue").val();
     

      var capacity = $("#capacity").val();

    if($('#insurer option:selected').val()!="none"  
      && $('#cat option:selected').val()!="none" 
      && $('#ctype option:selected').val()!="none"
      && $('#period option:selected').val()!="none"
      )
    {
 
      var insurer_id=$('#insurer option:selected').val();
      var cat_id=$('#cat option:selected').val();
      var ctype_id=$('#ctype option:selected').val();
      var period=$('#period option:selected').val()
      // var period=$('#period option:selected').val().split(',');

        $.ajax({
          type: "POST",
          url: 'motorsformular',
          data: "insurerid="+insurer_id+"&catid="+cat_id+"&ctypeid="+ctype_id,
          success: function(data) {
            console.log(data);
            $.each(JSON.parse(data),function(i,v)
               {

                var rate = (i,v.rate);
                var flat_rate = (i,v.flat_rate);
                var perseat = (i,v.perseat);
                var ebb = (i,v.ebb);
                var unex = (i,v.unexperience);
                var uage = (i,v.underage);

               
                if (flat_rate==0)
                {
                $('#value').show(400);


                var a=((rate)*value);
                var b=(flat_rate*1);
                var c=(perseat*capacity);

                var bp = period*(a + b + c);
                var x = period*(s_ebb*((ebb)*bp));
                var y = period*(s_unex*((unex)*bp));
                var z = period*(s_uage*((uage)*bp));



                $('#ebb').css('color','blue').val(x.toFixed(2));
                $('#unex').css('color','blue').val(y.toFixed(2));
                $('#uage').css('color','blue').val(z.toFixed(2));

                var mp = (bp + x + y + z);

                var vat = (mp*(18/100));
                var nnp = (mp + vat);
                $('#vat').css('color','blue').val(vat.toFixed(2));

                var np = (nnp).toFixed(2);  
                $('#npvalue').css('color','darkblue').val(np);
                $('#np').show(500);
                } 
                else
                {
                $('#value').hide(400);
                // var a=((rate)*value);
                var b=(flat_rate*1);
                var c=(perseat*capacity);

                // var bp = a + b + c;
                var bp =b + c;

                var x = s_ebb*((ebb)*bp);
                var y = s_unex*((unex)*bp);
                var z = s_uage*((uage)*bp);


               
                $('#ebb').css('color','blue').val(x.toFixed(2));
                $('#unex').css('color','blue').val(y.toFixed(2));
                $('#uage').css('color','blue').val(z.toFixed(2));

                var mp = period*(bp + x + y + z);


                var vat = (mp*(18/100));
                $('#vat').css('color','blue').val(vat.toFixed(2));


                var nnp = (mp + vat);
                var np = (nnp).toFixed(2);  
                $('#npvalue').css('color','darkblue').val(np);
                $('#np').show(500);
                };
               
             
              }

              );
          }
          });
        }
        else{
          $('#price').hide(500);
          
        }
    
  })
});



// pending
// date
$('#id,#period4').change(function(){


 if($('#period4 option:selected').val()!="none")
 {
      // var period = $('#period4 option:selected').val().split(',');

      // var a = period[1];
      // alert('/'+a);


      var s = $("#i_date").val();
      var startDate = s;
      var endDateMoment = moment(startDate);
      y = endDateMoment.add(2, 'months');
      var z = y.format("MM/DD/YYYY"); 
      $('#e_date').val(z);
  }

 else{


  $('#e_date').css('color','red').val('Select period!');
 }

});

// end date



// insurance report
// insurance covers
$(document).ready(function() 
{

$('#date_i,#date_e,#covertype,#insr_id').change(function()
  {

    if($('#date_i').val()!=0 && $('#date_e').val()!=0)
    
    {


        var i_date = moment($("#date_i").val()).format('YYYY-MM-DD');
        var e_date = moment($("#date_e").val()).format('YYYY-MM-DD');

        

        // alert(i_date+'/'+e_date);

      if($('#insr_id option:selected').val()!="none"  && $('#covertype option:selected').val()!="none")
          {
        
            var ctype = $("#covertype option:selected").val();
            var insr_id = $("#insr_id option:selected").val();
            var i_date = moment($("#date_i").val()).format('YYYY-MM-DD');
            var e_date = moment($("#date_e").val()).format('YYYY-MM-DD');
            // alert(insr_id+'/'+ctype);

                $.ajax({
                  type: "POST",
                  url: 'report_one',
                  data: "insurerid="+insr_id+"&ctypeid="+ctype+"&i_date="+i_date+"&e_date="+e_date,
                  success: function(data) {
                    // console.log(data);
                     // <--- Begining of report---->
                if (data !='false'){
                 
                       var trHTML = '';
                            $.each(JSON.parse(data),function(i,v) {
                               console.log(i,v);
                            
                            trHTML += 
                            '<tr class="active"><td>' + v.i_name + 
                            '</td><td>' + v.class + 
                            '</td><td>' + v.covernotetype + 
                            '</td><td>' + v.reg_no + 
                            '</td><td>' + v.icn + 
                            '</td><td>' + v.sticker_no + 
                            '</td><td>' + v.np + 
                            '</td></tr>';
                            });
                            $('#records_table').empty().hide().append(trHTML).show('slow');
                      }
                  else{
               
                      $('#records_table').empty().hide().append('There is no record for this case').show('slow');
                      } 
                  // <--- end of report---->
                  }
                  });
        }
     
        else if($('#insr_id option:selected').val()=="none"  && $('#covertype option:selected').val()!="none")
        {
          
            var ctype = $("#covertype option:selected").val();
            var i_date = moment($("#date_i").val()).format('YYYY-MM-DD');
            var e_date = moment($("#date_e").val()).format('YYYY-MM-DD');

                $.ajax({
                  type: "POST",
                  url: 'report_two',
                  data: "ctypeid="+ctype+"&i_date="+i_date+"&e_date="+e_date,
                  success: function(data) {
            
                  // console.log(data);
                  // <--- Begining of report---->
                if (data !='false'){
                 
                       var trHTML = '';
                            $.each(JSON.parse(data),function(i,v) {
                               console.log(i,v);
                            
                            trHTML += 
                            '<tr class="active"><td>' + v.i_name + 
                            '</td><td>' + v.class + 
                            '</td><td>' + v.covernotetype + 
                            '</td><td>' + v.reg_no + 
                            '</td><td>' + v.icn + 
                            '</td><td>' + v.sticker_no + 
                            '</td><td>' + v.np + 
                            '</td></tr>';
                            });
                            $('#records_table').empty().hide().append(trHTML).show('slow');
                      }
                  else{
               
                      $('#records_table').empty().hide().append('There is no record for this case').show('slow');
                      } 
                  // <--- end of report---->
                  }
                  });
        }
        else if($('#insr_id option:selected').val()!="none"  && $('#covertype option:selected').val()=="none")
        {

            var insr_id = $("#insr_id option:selected").val();
            var i_date = moment($("#date_i").val()).format('YYYY-MM-DD');
            var e_date = moment($("#date_e").val()).format('YYYY-MM-DD');

                $.ajax({
                  type: "POST",
                  url: 'report_three',
                  data: "insurerid="+insr_id+"&i_date="+i_date+"&e_date="+e_date,
                  success: function(data) {
                    // console.log(data);
                        // <--- Begining of report---->
                if (data !='false'){
                 
                       var trHTML = '';
                            $.each(JSON.parse(data),function(i,v) {
                               console.log(i,v);
                            
                            trHTML += 
                            '<tr class="active"><td>' + v.i_name + 
                            '</td><td>' + v.class + 
                            '</td><td>' + v.covernotetype + 
                            '</td><td>' + v.reg_no + 
                            '</td><td>' + v.icn + 
                            '</td><td>' + v.sticker_no + 
                            '</td><td>' + v.np + 
                            '</td></tr>';
                            });
                            $('#records_table').empty().hide().append(trHTML).show('slow');
                      }
                  else{
               
                      $('#records_table').empty().hide().append('There is no record for this case').show('slow');
                      } 
                  // <--- end of report---->

                  }
                  });
        }
        else{
            var i_date = moment($("#date_i").val()).format('YYYY-MM-DD');
            var e_date = moment($("#date_e").val()).format('YYYY-MM-DD');
           $.ajax({
                  type: "POST",
                  url: 'report_four',
                  data: "i_date="+i_date+"&e_date="+e_date,
                  success: function(data) {
                    // console.log(data);
                      // <--- Begining of report---->
                if (data !='false'){
                 
                       var trHTML = '';
                            $.each(JSON.parse(data),function(i,v) {
                               console.log(i,v);
                            
                            trHTML += 
                            '<tr class="active"><td>' + v.i_name + 
                            '</td><td>' + v.class + 
                            '</td><td>' + v.covernotetype + 
                            '</td><td>' + v.reg_no + 
                            '</td><td>' + v.icn + 
                            '</td><td>' + v.sticker_no + 
                            '</td><td>' + v.np + 
                            '</td></tr>';
                            });
                            $('#records_table').empty().hide().append(trHTML).show('slow');
                      }
                  else{
               
                      $('#records_table').empty().hide().append('There is no record for this case').show('slow');
                      } 
                  // <--- end of report---->
                  }
                  });
        };

        }
        else{
          // select date interval
          
        }
    
  })
});


// end of insurer covernotes report


// insurance coverages
$(document).ready(function() 
{

$('#c_date_i,#c_date_e,#c_covertype,#c_insr_id').change(function()
  {

    if($('#c_date_i').val()!=0 && $('#c_date_e').val()!=0)
    
    {


        var i_date = moment($("#c_date_i").val()).format('YYYY-MM-DD');
        var e_date = moment($("#c_date_e").val()).format('YYYY-MM-DD');

        

        // alert(i_date+'/'+e_date);

      if($('#c_insr_id option:selected').val()!="none"  && $('#c_covertype option:selected').val()!="none")
          {
        
            var ctype = $("#c_covertype option:selected").val();
            var insr_id = $("#c_insr_id option:selected").val();
            var i_date = moment($("#c_date_i").val()).format('YYYY-MM-DD');
            var e_date = moment($("#c_date_e").val()).format('YYYY-MM-DD');
            // alert(insr_id+'/'+ctype);

                $.ajax({
                  type: "POST",
                  url: 'report_one',
                  data: "insurerid="+insr_id+"&ctypeid="+ctype+"&i_date="+i_date+"&e_date="+e_date,
                  success: function(data) {
                    console.log(data);
                     // <--- Begining of report---->
                if (data !='false'){
                 
                       var trHTML = '';
                            $.each(JSON.parse(data),function(i,v) {
                               console.log(i,v);
                            
                            trHTML += 
                            '<tr class="active"><td>' + v.i_name + 
                            '</td><td>' + v.class + 
                            '</td><td>' + v.np + 
                            '</td><td>' + v.br +  
                            '</td></tr>';
                            });
                            $('#c_records_table').empty().hide().append(trHTML).show('slow');
                      }
                  else{
               
                      $('#c_records_table').empty().hide().append('There is no record for this case').show('slow');
                      } 
                  // <--- end of report---->
                  }
                  });
        }
     
        else if($('#c_insr_id option:selected').val()=="none"  && $('#c_covertype option:selected').val()!="none")
        {
          
            var ctype = $("#c_covertype option:selected").val();
            var i_date = moment($("#c_date_i").val()).format('YYYY-MM-DD');
            var e_date = moment($("#c_date_e").val()).format('YYYY-MM-DD');

            // alert(ctype);

                $.ajax({
                  type: "POST",
                  url: 'report_two',
                  data: "ctypeid="+ctype+"&i_date="+i_date+"&e_date="+e_date,
                  success: function(data) {
            
                  console.log(data);
                  // <--- Begining of report---->
                if (data !='false'){
                 
                       var trHTML = '';
                            $.each(JSON.parse(data),function(i,v) {
                               console.log(i,v);
                            
                            trHTML += 
                            '<tr class="active"><td>' + v.i_name + 
                            '</td><td>' + v.class + 
                            '</td><td>' + v.np + 
                            '</td><td>' + v.br +  
                            '</td></tr>';
                            });
                            $('#c_records_table').empty().hide().append(trHTML).show('slow');
                      }
                  else{
               
                      $('#c_records_table').empty().hide().append('There is no record for this case').show('slow');
                      } 
                  // <--- end of report---->
                  }
                  });
        }
        else if($('#c_insr_id option:selected').val()!="none"  && $('#c_covertype option:selected').val()=="none")
        {

            var insr_id = $("#c_insr_id option:selected").val();
            var i_date = moment($("#c_date_i").val()).format('YYYY-MM-DD');
            var e_date = moment($("#c_date_e").val()).format('YYYY-MM-DD');

            // alert(insr_id);
                $.ajax({
                  type: "POST",
                  url: 'report_three',
                  data: "insurerid="+insr_id+"&i_date="+i_date+"&e_date="+e_date,
                  success: function(data) {
                    console.log(data);
                        // <--- Begining of report---->
                if (data !='false'){
                 
                       var trHTML = '';
                            $.each(JSON.parse(data),function(i,v) {
                               console.log(i,v);
                            
                            trHTML += 
                           '<tr class="active"><td>' + v.i_name + 
                            '</td><td>' + v.class + 
                            '</td><td>' + v.np + 
                            '</td><td>' + v.br +  
                            '</td></tr>';
                            });
                            $('#c_records_table').empty().hide().append(trHTML).show('slow');
                      }
                  else{
               
                      $('#c_records_table').empty().hide().append('There is no record for this case').show('slow');
                      } 
                  // <--- end of report---->

                  }
                  });
        }
        else{
            var i_date = moment($("#c_date_i").val()).format('YYYY-MM-DD');
            var e_date = moment($("#c_date_e").val()).format('YYYY-MM-DD');
           $.ajax({
                  type: "POST",
                  url: 'report_four',
                  data: "i_date="+i_date+"&e_date="+e_date,
                  success: function(data) {
                    console.log(data);
                      // <--- Begining of report---->
                if (data !='false'){
                 
                       var trHTML = '';
                            $.each(JSON.parse(data),function(i,v) {
                               console.log(i,v);
                            
                            trHTML += 
                            '<tr class="active"><td>' + v.i_name + 
                            '</td><td>' + v.class + 
                            '</td><td>' + v.np + 
                            '</td><td>' + v.br +  
                            '</td></tr>';
                            });
                            $('#c_records_table').empty().hide().append(trHTML).show('slow');
                      }
                  else{
               
                      $('#c_records_table').empty().hide().append('There is no record for this case').show('slow');
                      } 
                  // <--- end of report---->
                  }
                  });
        };

        }
        else{
          // select date interval
          
        }
    
  })
});
// end of coverage report
// end of insurance report
