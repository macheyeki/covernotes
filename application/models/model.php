<?php 

	class Model extends CI_Model
	{
		

		public function addBeneficiries($data)
		{
			$this->db->insert('beneficiary',$data);
			
		}




		function getInsurers()
		{
		$this->db->where('id!=',1);
		$query = $this->db->get('insurers');
		return  $query->result();
		}
		function getCovernotestypes()
		{
		$query = $this->db->get('cover_notes_types');
		return  $query->result();
		}
		function getMotorscategory()
		{
		$query = $this->db->get('motors_category');
		return  $query->result();
		}
		function getPeriod()
		{
		$query = $this->db->get('period');
		return  $query->result();
		}
		function getMotorsformular($insurer_id,$cat_id,$ctype_id)
		{
			$this->db->where('insurer_id',$insurer_id);
			$this->db->where('motorcat_id',$cat_id);
			$this->db->where('covertype_id',$ctype_id);
			$results=$this->db->get('motors_formular');

			if($results->num_rows() > 0)
			{
    			return $results->result();
    		}
    		else{
    			return false;
    		} 
		}

		// report

		function getReports()
		{
		$query = $this->db->get('covernotes_view');
		return  $query->result();
		}


		function reportOne($i_date,$e_date,$insurer_id,$ctype_id)
		{
			$this->db->where('i_date >=',$i_date);
			$this->db->where('i_date <=',$e_date);
			$this->db->where('insurer_id',$insurer_id);
			$this->db->where('covertype_id',$ctype_id);
			$results=$this->db->get('covernotes_view');

			if($results->num_rows() > 0)
			{
    			return $results->result();
    		}
    		else{
    			return false;
    		} 
		}
		function reportTwo($i_date,$e_date,$ctype_id)
		{
			$this->db->where('i_date >=',$i_date);
			$this->db->where('i_date <=',$e_date);
			$this->db->where('covertype_id',$ctype_id);
			$results=$this->db->get('covernotes_view');

			if($results->num_rows() > 0)
			{
    			return $results->result();
    		}
    		else{
    			return false;
    		} 
		}
		function reportThree($i_date,$e_date,$insurer_id)
		{
			$this->db->where('i_date >=',$i_date);
			$this->db->where('i_date <=',$e_date);
			$this->db->where('insurer_id',$insurer_id);
			$results=$this->db->get('covernotes_view');

			if($results->num_rows() > 0)
			{
    			return $results->result();
    		}
    		else{
    			return false;
    		} 
		}
		function reportFour($i_date,$e_date)
		{
			$this->db->where('i_date >=',$i_date);
			$this->db->where('i_date <=',$e_date);
			$results=$this->db->get('covernotes_view');

			if($results->num_rows() > 0)
			{
    			return $results->result();
    		}
    		else{
    			return false;
    		} 
		}
		// end report
		function getClasses()
		{
		$query = $this->db->get('classes');
		return  $query->result();
		}

		

		

	}
?>