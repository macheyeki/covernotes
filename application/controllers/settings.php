<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	function __construct()
		{
			parent::__construct();
			$this->load->model('model');
		}



	public function classDefinition()
	{	
		if(isset($_GET['c']))
		{
				if($_GET['c']=='view'){
				$data['title']='';
					$data['classes'] = $this->model->getClasses();
					$data['settings']='settings/classes/classes_view';
					$data['contents'] = 'settings/classes/classes_all';
					$this->load->view('layout/master',$data);
			

				}
				elseif($_GET['c']=='add')
				{
					// validation
				$this->form_validation->set_rules('class','Insurance class','trim|required');
				$this->form_validation->set_rules('brokerage','brokerage %','trim|required');
					if ($this->form_validation->run()==FALSE)
		 			{
		 	

			

					$data['title']='';
					$data['settings']='settings/classes/classes_add';
					$data['contents'] = 'settings/classes/classes_all';
					$this->load->view('layout/master',$data);

					}

				else{
					$class=ucfirst($this->input->post('class'));	
					$brokerage=$this->input->post('brokerage');	
					$data = array(
					'class' => $class,
					'brokerage' =>$brokerage
					);
				
					$this->db->insert('classes',$data);
					$this->session->set_flashdata('message', 'Insurance class have been added successifully');

					redirect('setting_classes?c=add');

				}
				}
			

			}
}
	public function covernotesDefinition()
	{	
		if(isset($_GET['cn']))
		{
				if($_GET['cn']=='view'){
					$data['title']='';

		// $data['categories'] = $this->model->getMotorscategory();
		// $data['insurers'] = $this->model->getInsurers();
		// $data['covernotestypes'] = $this->model->getCovernotestypes();
					$data['settings']='settings/covernotes/view';
					$data['contents'] = 'settings/covernotes/covernotes_all';
					$this->load->view('layout/master',$data);
			

				}
				elseif($_GET['cn']=='add')
				{
					// validation
<<<<<<< HEAD
					$this->form_validation->set_rules('rate','rate','trim|required');
					$this->form_validation->set_rules('perseat','total seat','trim|required');
					$this->form_validation->set_rules('flat_rate','flat rate','trim|required');
					// $this->form_validation->set_rules('category','category','trim|required');
					// $this->form_validation->set_rules('insurer','insuarer','trim|required');
					// $this->form_validation->set_rules('covernotestypes','covernotes types','trim|required');
=======
					$this->form_validation->set_rules('beneficiary','Beneficiary name','trim|required');
					$this->form_validation->set_rules('email','Email','trim|valid_email|required');
					$this->form_validation->set_rules('mobile','Mobile number','trim|required');
>>>>>>> be2b67568ecfdbc910cf15bf33f0302f9ea7bf72

					if ($this->form_validation->run()==FALSE)
		 			{
		 	

			

					$data['title']='';
<<<<<<< HEAD
					$data['categories']= $this->model->getMotorscategory();
					$data['covernotestypes']= $this->model->getCovernotestypes();
					$data['insurers']= $this->model->getInsurers();

=======
>>>>>>> be2b67568ecfdbc910cf15bf33f0302f9ea7bf72
					$data['settings']='settings/covernotes/settings';
					$data['contents'] = 'settings/covernotes/covernotes_all';
					$this->load->view('layout/master',$data);

					}

				else{
<<<<<<< HEAD
					$covernotestypes=$this->input->post('covernotestype');	
					$insurer=$this->input->post('insurer_name');
					$category=$this->input->post('category');
					$rate=$this->input->post('rate');
					$flat=$this->input->post('flat_rate');
					$perseat=$this->input->post('perseat');
						
					$data = array(
					'covertype_id' => $covernotestypes,
					'insurer_id' =>$insurer,
					'motorcat_id' =>$category,
					'rate' =>$rate,
					'flat_rate' =>$flat,
					'perseat' =>$perseat
					);
				
					$this->db->insert('motors_formular',$data);
					$this->session->set_flashdata('message', 'data has been added successfuly');
					 //$this->load->model('',$data);
					// $this->model->addBeneficiary($data);

					redirect('setting_covernotes?cn=add');
=======
					$beneficiary=ucfirst($this->input->post('beneficiary'));	
					$email=$this->input->post('email');
					
					$mobile=$this->input->post('mobile');
						
					$data = array(
					'name' => $beneficiary,
					'email' =>$email,
					'mobile_no' =>$mobile
					);
				
					$this->db->insert('beneficiaries',$data);
					$this->session->set_flashdata('message', 'Beneficiary details have been added successifully');
					// $this->load->model('MyModel',$data);
					// $this->model->addBeneficiary($data);

					redirect('insurers/insurers?i=add');
>>>>>>> be2b67568ecfdbc910cf15bf33f0302f9ea7bf72

				}
				}
			

			}
}
	
	public function notifications()
	{	
		
		 	
			$data['title']='';
			$data['contents'] = 'settings/notifications';
			$this->load->view('layout/master',$data);

			
	}
	public function classEnable()
	{	
		
	if(isset($_POST['ajax']))
			{

				$status=explode("|", $_POST['status']);
				$value=($status[1]=='checked')?75:50;
				$id=($status[0]);

				$this->db->set('status', $value);
				$this->db->where('id', $id);
				$this->db->update('classes');
		   		return "succeed";
				
			}

			else
			{
		    echo "yes";
		   }
			
	}



}
