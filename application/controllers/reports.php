<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH."/third_party/PHPExcel.php";

class Reports extends CI_Controller {

function __construct()
		{
			parent::__construct();
			$this->load->model('model');
             $this->load->library('excel');
             $real = realpath('C:/wamp/www/covernotes');
		}

	
	public function coversReports()
	{
        $data['reports'] = $this->model->getReports();
        $data['insurers'] = $this->model->getInsurers();
        $data['covernotestypes'] = $this->model->getCovernotestypes();
		$data['contents'] = 'reports/covernotes';
		$this->load->view('layout/master',$data);
	}
	public function coverageReports()
    {
        $data['reports'] = $this->model->getReports();
        $data['insurers'] = $this->model->getInsurers();
        $data['covernotestypes'] = $this->model->getCovernotestypes();
        $data['contents'] = 'reports/coverage';
        $this->load->view('layout/master',$data);
    }


    // reports
    public function reportOne()
    {
        $i_date = $this->input->post('i_date');
        $e_date = $this->input->post('e_date');
        $ctype_id=$this->input->post('ctypeid');
        $insurer_id=$this->input->post('insurerid');
   
        echo (json_encode($this->model->reportTwo($i_date,$e_date,$ctype_id,$insurer_id)));
    }
    public function reportTwo()
    {
        $i_date = $this->input->post('i_date');
        $e_date = $this->input->post('e_date');
        $ctype_id=$this->input->post('ctypeid');

        echo (json_encode($this->model->reportTwo($i_date,$e_date,$ctype_id)));
    }
    public function reportThree()
    {
        $i_date = $this->input->post('i_date');
        $e_date = $this->input->post('e_date');
        $insurer_id=$this->input->post('insurerid');

        echo (json_encode($this->model->reportThree($i_date,$e_date,$insurer_id)));
    }
    public function reportFour()
    {
        $i_date = $this->input->post('i_date');
        $e_date = $this->input->post('e_date');


        echo (json_encode($this->model->reportFour($i_date,$e_date)));
    }

       
    // end reports

    public function allCovernotes()
    {
                $this->excel->setActiveSheetIndex(0);
                //name the worksheet
                $this->excel->getActiveSheet()->setTitle('cover_notes');
                //set cell A1 content with some text
                $this->excel->getActiveSheet()->setCellValue('A1', 'List of cover notes');
                $this->excel->getActiveSheet()->setCellValue('A3', 'INSURED NAME');
                $this->excel->getActiveSheet()->setCellValue('B3', 'CLASS');
                $this->excel->getActiveSheet()->setCellValue('C3', 'REG NO');
                $this->excel->getActiveSheet()->setCellValue('D3', 'ICN NO');
                $this->excel->getActiveSheet()->setCellValue('E3', 'STICKER NO');
                $this->excel->getActiveSheet()->setCellValue('F3', 'MOBILE');
                $this->excel->getActiveSheet()->setCellValue('G3', 'EMAIL');
                $this->excel->getActiveSheet()->setCellValue('H3', 'MAKE');
                $this->excel->getActiveSheet()->setCellValue('I3', 'BODY');
                $this->excel->getActiveSheet()->setCellValue('J3', 'YOM');
                $this->excel->getActiveSheet()->setCellValue('K3', 'VAT');
                $this->excel->getActiveSheet()->setCellValue('L3', 'NET PRIMIUM');
                //merge cell A1 until L1
                $this->excel->getActiveSheet()->mergeCells('A1:L1');
                   //merge cell A2 until L2
                $this->excel->getActiveSheet()->mergeCells('A2:L2');

                //set aligment to LEFT for that merged cell (A1 to L1)
                $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                //make the font become bold
                $this->excel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('B3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('C3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('D3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('E3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('F3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('G3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('H3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('I3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('J3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('K3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('L3')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
                $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#000080');
       for($col = ord('A'); $col <= ord('L'); $col++){
                //set column dimension
                $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
                 //change the font size
                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                 
                $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        }
                //retrive cover notes table data
                $this->db->where('i_date >=',$i_date);
                $this->db->where('i_date <=',$e_date);
                $this->db->where('covertype_id',$ctype_id);
                
                 $this->db->select('i_name,class,reg_no,icn,sticker_no,mobile,email,make,body,yom,vat,np');
                
                $rs = $this->db->get('covernotes_view');
                // $rs = $this->db->get('cover_notes_view');
                $exceldata="";
                foreach ($rs->result_array() as $row){
                $exceldata[] = $row;

              
        }
                //Fill data 
                $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A4');
                 
                $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $this->excel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                 
                $filename= mt_rand(1,100000).'.xls'; //just some random filename
                 //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
 
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');
                // save to folder
                $objWriter->save('export/'.$filename);
             exit; //done.. exiting!   
    }

	function repot()
    {
        $query = $this->db->get('cover_notes');
        if(!$query)
            return false;
        // Starting the PHPExcel library




        $this->load->library('PHPExcel');
        $this->load->library('PHPExcel/IOFactory');
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
        // Fetching the table data
        $row = 2;
        foreach($query->result() as $data)
        {
            $col = 0;
            foreach ($fields as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
            $row++;
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Products_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save("nameoffile.xls");
        $objWriter->save('php://output');
    }
   }