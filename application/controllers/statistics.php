<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistics extends CI_Controller {
function __construct()
		{
			parent::__construct();
			
		}

	
	public function statistics()
	{
		$data['contents'] = 'statistics/statistics';
		$this->load->view('layout/master',$data);
	}
}
