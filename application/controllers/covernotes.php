<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Covernotes extends CI_Controller {

	function __construct()
		{
			parent::__construct();
			$this->load->model('model');

		}



	public function coverNots()
	{
		// validation
		$this->form_validation->set_rules('cover_no','Cover number','trim|required');
		$this->form_validation->set_rules('f_name','Customer first name','trim|required');
		$this->form_validation->set_rules('l_name','Customer last name','trim|required');
		$this->form_validation->set_rules('mobile_no','Mobile number','trim|required');
		$this->form_validation->set_rules('email','Email','trim|valid_email|required');
		$this->form_validation->set_rules('vehicle_name','vehicle name','trim|required');
		$this->form_validation->set_rules('vehicle_no','vehicle number','trim|required');
		// $this->form_validation->set_rules('covertype_id','Cover type','trim|required|callback_select_validate');
		// $this->form_validation->set_rules('beneficiary_id', 'beneficiary', 'required|callback_select_validate');
		// $this->form_validation->set_message('beneficiary_id', 'You need to select something other than the default');
		// |callback_select_validate'

		// $this->form_validation->set_rules('first_date','First date','trim|required');
		// $this->form_validation->set_rules('expire_date','expire date','trim|required');



		if ($this->form_validation->run()==FALSE)
		 {

		$data['beneficiaries'] = $this->model->getBeneficiaries();
		$data['covernotestypes'] = $this->model->getCovernotestypes();
		$data['contents'] = 'registration/cover_notes';
		$this->load->view('layout/master',$data);
		}
		else
		{
				$coverno=$this->input->post('cover_no');
				$covertype_id=$this->input->post('covertype_id');
				$beneficiary_id=$this->input->post('beneficiary_id');

				$fname=ucfirst($this->input->post('f_name'));	
				$lname=ucfirst($this->input->post('l_name'));	
				$mobile=$this->input->post('mobile_no');	
				$email=$this->input->post('email');

				$vehicle_name=ucfirst($this->input->post('vehicle_name'));	
				$vehicle_no=$this->input->post('vehicle_no');

			
				$data = array(
				'cover_notes_type_id' => $covertype_id,
				'beneficiary_id' =>$beneficiary_id,
				'cover_no' =>$coverno,
				'customer_fname' =>$fname,
				'customer_lname' =>$lname,
				'mobile_no' =>$mobile,
				'email' =>$email,
				'vehicle_name' =>$vehicle_name,
				'vehicle_no' =>$vehicle_no,
				'first_date' =>2015-10-10,
				'expire_date' =>2016-10-09
				);

				var_dump($data);
				
				$this->db->insert('cover_notes',$data);
				$this->session->set_flashdata('message', 'Hello! Cover notes details have been added successifully');
				// // $this->load->model('MyModel',$data);
				// // $this->model->addBeneficiary($data);

				// redirect('covernotes');
				$this->session->set_flashdata('mail',$email);	
				redirect('mailcustomer');

		}
	}

	public function coverNotestypes()
	{	
		// validation
			$this->form_validation->set_rules('covernote','Covernotes name','trim|required');

			if ($this->form_validation->run()==FALSE)
		 	{
		 	
			$data['title']='';
			$data['contents'] = 'registration/cover_notestypes';
			$this->load->view('layout/master',$data);

			}
			else{
				$covernotestype=ucfirst($this->input->post('covernote'));	
						
				$data = array(
				'type' => $covernotestype
				);
				
				$this->db->insert('cover_notes_types',$data);
				$this->session->set_flashdata('message', 'A covernote type has been added successifully');
				// $this->load->model('MyModel',$data);
				// $this->model->addBeneficiary($data);

				redirect('covernotes_types');

			}
	}

	public function beneficiaries()
	{	
		// validation
			$this->form_validation->set_rules('beneficiary','Beneficiary name','trim|required');
			$this->form_validation->set_rules('email','Email','trim|valid_email|required');
			$this->form_validation->set_rules('mobile','Mobile number','trim|required');

			if ($this->form_validation->run()==FALSE)
		 	{
		 	
			$data['title']='';
			$data['contents'] = 'registration/beneficiaries';
			$this->load->view('layout/master',$data);

			}
			else{
				$beneficiary=ucfirst($this->input->post('beneficiary'));	
				$email=$this->input->post('email');
				$mobile=$this->input->post('mobile');
						
				$data = array(
				'name' => $beneficiary,
				'email' =>$email,
				'mobile_no' =>$mobile
				);
				
				$this->db->insert('beneficiaries',$data);
				$this->session->set_flashdata('message', 'Beneficiary details have been added successifully');
				// $this->load->model('MyModel',$data);
				// $this->model->addBeneficiary($data);

				redirect('beneficiaries');

			}
	}
	public function covernotes()
	{	
		
		 	
			$data['title']='';
			$data['categories'] = $this->model->getMotorscategory();
			$data['insurers'] = $this->model->getInsurers();
			$data['period'] = $this->model->getPeriod();
			$data['covernotestypes'] = $this->model->getCovernotestypes();
			$data['contents'] = 'insurance/insurance_add';
			$this->load->view('layout/master',$data);

			
	}
	public function getMotorsformular()
	{
		$insurer_id=$this->input->post('insurerid');
		$cat_id=$this->input->post('catid');
		$ctype_id=$this->input->post('ctypeid');
		$id=$this->input->post('id');

		echo (json_encode($this->model->getMotorsformular($insurer_id,$cat_id,$ctype_id)));
	}

	public function motorsAdd()
	{
		// validation
		$this->form_validation->set_rules('f_name','First name','trim|required');
		$this->form_validation->set_rules('l_name','Last name','trim|required');

		$this->form_validation->set_rules('mobile','Mobile number','trim|required');
		$this->form_validation->set_rules('box','Post box','trim|required');
		$this->form_validation->set_rules('email','Email','trim|valid_email|required');

		$this->form_validation->set_rules('make','Make','trim|required');
		$this->form_validation->set_rules('regno','Reg number','trim|required');


		$this->form_validation->set_rules('e_no','Engine NO','trim|required');
		$this->form_validation->set_rules('c_no','Chasis NO','trim|required');

		$this->form_validation->set_rules('body','Body','trim|required');
		$this->form_validation->set_rules('cc','CC','trim|required');

		$this->form_validation->set_rules('yom','YOM','trim|required');
		$this->form_validation->set_rules('i_date','First date','trim|required');
		$this->form_validation->set_rules('e_date','Expiire date','trim|required');
		$this->form_validation->set_rules('capacity','Capacity','trim|required');
	
		$this->form_validation->set_rules('icn','ICN','trim|required');
		$this->form_validation->set_rules('stickerno','Sticker NO','trim|required');
		// $this->form_validation->set_rules('value','Sum isured','trim|required');



		if ($this->form_validation->run()==FALSE)
		 {


		$data['title']='';
		$data['categories'] = $this->model->getMotorscategory();
		$data['insurers'] = $this->model->getInsurers();
		$data['period'] = $this->model->getPeriod();
		$data['covernotestypes'] = $this->model->getCovernotestypes();
		$data['contents'] = 'insurance/insurance_add';
		$this->load->view('layout/master',$data);
		}
		else
		{

				$insurer_id=$this->input->post('insurer_name');
				$fname=ucfirst($this->input->post('f_name'));
				$mname=ucfirst($this->input->post('m_name'));	
				$lname=ucfirst($this->input->post('l_name'));	

				$mobile=$this->input->post('mobile');
				$box=$this->input->post('box');
				$email=$this->input->post('email');

				$make=$this->input->post('make');
				$regno=$this->input->post('regno');
				$e_no=$this->input->post('e_no');
				$c_no=$this->input->post('c_no');
				$body=$this->input->post('body');
				$cc=$this->input->post('cc');
				$yom=$this->input->post('yom');
				$capacity=$this->input->post('capacity');
				
				$i_date=$this->input->post('i_date');
				$e_date=$this->input->post('e_date');

				$idate = date("Y-m-d ", strtotime($i_date));
				$edate = date("Y-m-d", strtotime($e_date));

				$value=$this->input->post('value');
				$icn=$this->input->post('icn');

				$stickerno=$this->input->post('stickerno');
				$covertype_id=$this->input->post('covernotestype');


				$category_id=$this->input->post('category');
				// $fla_rate=$this->input->post('category[1]');

				$ebb_s=$this->input->post('ebb');
				$un_s=$this->input->post('unexperienced');
				$uage_s=$this->input->post('underage');
				$period_id=$this->input->post('period');
			 // $period=$this->input->post('period[1]');
				$capacity=$this->input->post('capacity');
				// Getting periods
				$this->db->where('id',$period_id);
				 $periods=$this->db->get('period')->result();; 
			
				foreach ($periods as $period) {
				
					 $period_rate= $period->percent;
					 $period_name=$period->period;

				}
				// Getting brokerage
				$this->db->where('id',1);
				 $results=$this->db->get('classes')->result();; 
			
				foreach ($results as $result) {
					 $brokerage= $result->brokerage;
					
				}

				// Getting formular
				$this->db->where('covertype_id',$covertype_id);
				$this->db->where('insurer_id',$insurer_id);
				$this->db->where('motorcat_id',$category_id);
				 $results=$this->db->get('motors_formular')->result();; 
			
				foreach ($results as $result) {
					$rate= $result->rate;
					$flat_rate= $result->flat_rate;
					$perseat= $result->perseat;
					$ebb=$result->ebb;
					$uage=$result->underage;
					$unex=$result->unexperience;
					
				}

			
				

				// Calculation
				// Basic premium
				$bp=$period_rate*(($rate * $value) + 1*($flat_rate) + ($capacity*$perseat));
				// Excess buy back
				$a=$ebb_s*($ebb*$bp);
				// Under age driver(<24)
				$b=$uage_s*($uage*$bp);
				// Unexperienced driver
				$c=$un_s *($unex*$bp);
				// Medium premium
				$mp=($bp + $a + $b + $c);
				// VAT
				$vat=(0.18*$mp);
				// Net premium
				$np=($mp + $vat);

				// Brokerage
				$br=$brokerage*$mp;


		
			
				$data1 = array(
				'covernotetype_id' => $covertype_id,
				'insurer_id' =>$insurer_id,
				'category_id' =>$category_id,
				'period_id' =>$period_id,
				'class_id' =>1,
				'reg_no' =>$regno,
				'icn_no' =>$icn,
				'insured_fname' =>$fname,
				'insured_mname' =>$mname,
				'insured_lname' =>$lname,
				'address' =>$box,
				'mobile' =>$mobile,
				'email' =>$email,

				'make' =>$make,
				'engine_no' =>$e_no,
				'chasis_no' =>$c_no,
				'bodytype' =>$body,
				'cc' =>$cc,
				'yom' =>$yom,
				'sticker_no' =>$stickerno,
				'capacity' =>$capacity,
				'i_date' => $idate,
				'e_date' => $edate,
				'yom' => $yom
				);
				
	// var_dump($data1);
				$this->db->insert('motor_covernotes',$data1);
				$covernote_id=$this->db->insert_id();

				$data2 = array(
				'class_id'=>1,
				'basic_premium'=> $bp,
				'ebb' => $a,
				'unexperience' => $c,
				'underage'=> $b,
				'vat'=> $vat,
				'net_premium'=> $np,
				'brokerage_rate'=>$brokerage,
				'brokerage'=> $br,
				'cover_id'=>$covernote_id);


				$this->db->insert('premium',$data2);

// var_dump($data2);
				$this->session->set_flashdata('message', 'Hello! Cover notes details have been added successifully');
				// // $this->load->model('MyModel',$data);
				// // $this->model->addBeneficiary($data);

				// +redirect('covernotes');
				$this->session->set_flashdata('mail',$email);	
				$this->session->set_flashdata('icn',$icn);	
				$this->session->set_flashdata('np',$np);		
				$this->session->set_flashdata('fname',$fname);
				$this->session->set_flashdata('lname',$lname);	
				$this->session->set_flashdata('period',$period_name);	
				$this->session->set_flashdata('i_date',$i_date);
				$this->session->set_flashdata('e_date',$e_date);	
				redirect('mailcustomer');
// // 
		
	
		}
}
}