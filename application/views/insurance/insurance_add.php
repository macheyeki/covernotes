
  <section>
  <div class="content-box-large">
   
    <div class="panel-body">
    <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home"><h5>Motors Vehicle</h5></a></li>
    <li><a data-toggle="tab" href="#menu1"><h6>Contractors</h6></a></li>
    <li><a data-toggle="tab" href="#menu2"><h6>Housing</h6></a></li>
    <li><a data-toggle="tab" href="#menu3"><h6>Accident</h6></a></li>
  </ul>

  <div class="tab-content" style="border:black 1px solid; border-radius:0 0 3px 3px;">
    <div id="home" class="tab-pane fade in active">
   
    
    <div class="panel-body">

    <form class="form-horizontal" role="form"action="<?php echo base_url("addmotors") ?>" method="post">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <div class="col-sm-offset-4 col-sm-6">
          <?php echo $this->session->flashdata('message'); ?>
        </div>
      
      </div>
      <div class="form-group">
        <label  class="col-sm-2 control-label">Insurer</label>
        <div class="col-sm-7">
          <select class="input form-control  input-sm selectpicker" name="insurer_name" id="insurer">
          <option value="none" selected="selected">Select insurer</option>
          <?php foreach($insurers as $insurer):?>
          <option value="<?php echo $insurer->id?>" <?php echo set_select('insurer_name', $insurer->id, False); ?>>
          <?php echo $insurer->name?></option>
          <?php endforeach;?>
          </select>
          <?php echo form_error('insurer'); ?>
        </div>
      </div>
   
    </div>
  </div>
    <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label  class="col-sm-2 control-label">Full Name</label>
        <div class="col-sm-3">
        <input type="text" class="form-control input input-sm"  name="f_name" value="<?php echo set_value('f_name')?>" placeholder="First name">
        <?php echo form_error('f_name'); ?>
        </div>
        <div class="col-sm-4">
        <input type="text" class="form-control input input-sm"  name="m_name" value="<?php echo set_value('m_name')?>" placeholder="Middle name">
        <?php echo form_error('m_name'); ?>
        </div>
        <div class="col-sm-3">
        <input type="text" class="form-control input input-sm"  name="l_name" value="<?php echo set_value('l_name')?>" placeholder="Last name">
        <?php echo form_error('l_name'); ?>
        </div>
      </div>
    </div>
  </div>
      <div class="row">
    <div class="col-md-12">
      <div class="form-group">
        <label  class="col-sm-2 control-label">Contacts</label>
        <div class="col-sm-3">
        <input type="text" class="form-control input input-sm"  name="mobile" value="<?php echo set_value('mobile')?>" placeholder="Mobile number">
        <?php echo form_error('mobile'); ?>
        </div>
        <div class="col-sm-4">
        <input type="email" class="form-control input input-sm"  name="email" value="<?php echo set_value('email')?>" placeholder="Email">
        <?php echo form_error('email'); ?>
        </div>
        <div class="col-sm-3">
        <input type="text" class="form-control input input-sm"  name="box" value="<?php echo set_value('box')?>" placeholder="Post box">
        <?php echo form_error('box'); ?>
        </div>
      </div><hr/>
    </div>

  </div>



  <!-- Vehicle's details -->
<div class="row">
    <div class="col-md-4" style="border-right:1px solid gray;">
     <div class="form-group">
        <label class="col-sm-6 control-label">Capacity</label>
        <div class="col-sm-6">
        <input type="number" class="form-control input input-sm" name="capacity" value="<?php echo set_value('capacity')?>" id="capacity" placeholder="Carring capacity">
        <?php echo form_error('capacity'); ?>
        </div>
      </div>
          <div class="form-group">
        <label  class="col-sm-6 control-label">Reg No</label>
        <div class="col-sm-6">
        <input type="text" class="form-control input input-sm"  name="regno" value="<?php echo set_value('regno')?>" placeholder="TXXX YYY">
        <?php echo form_error('regno'); ?>
        </div>
      </div>
            <div class="form-group">
        <label  class="col-sm-6 control-label">Make</label>
        <div class="col-sm-6">
        <input type="text" class=" form-control input input-sm"  name="make" value="<?php echo set_value('make')?>" placeholder="Make">
        <?php echo form_error('make'); ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-6 control-label">Engine NO</label>
        <div class="col-sm-6">
        <input type="text" class="form-control input input-sm" name="e_no" value="<?php echo set_value('e_no')?>" placeholder="Engine number">
        <?php echo form_error('e_no'); ?>
        </div>
      </div>
      <div class="form-group">
        <label for="inputPassword6" class="col-sm-6 control-label">Chasis No</label>
        <div class="col-sm-6">
        <input type="text" class="form-control input input-sm" name="c_no" value="<?php echo set_value('c_no')?>" placeholder="Chasis number">
        <?php echo form_error('c_no'); ?>
        </div>
      </div>  
      <div class="form-group">
        <label  class="col-sm-6 control-label">Body type</label>
        <div class="col-sm-6">
        <input type="text" class="form-control input input-sm"  name="body" value="<?php echo set_value('body')?>" placeholder="Body type">
        <?php echo form_error('body'); ?>
        </div>
      </div>
      <div class="form-group">
        <label class="col-sm-6 control-label">CC</label>
        <div class="col-sm-6">
        <input type="text" class="form-control input input-sm" name="cc" value="<?php echo set_value('cc')?>" placeholder="CC">
        <?php echo form_error('cc'); ?>
        </div>
      </div>
    
      <div class="form-group">
        <label  class="col-sm-6 control-label">YOM</label>
      
         <div class="col-sm-6">
    <div class='input-group start date'>
            <input type='text'  name="yom" class="form-control  input-sm" value="<?php echo set_value('yom')?>" placeholder="Year of manufacture" readonly/>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            </span>
            <?php echo form_error('yom'); ?>
        </div>
    </div>
      </div>
      
          
        </div>


        <!-- insurance details -->
      <div class="col-md-4" style="border-right:1px solid gray;">
       <div class="form-group">
        <label  class="col-sm-6 control-label">Category</label>
        <div class="col-sm-6">
        <select class="input form-control  input-sm selectpicker" name="category" id="cat">
          <option value="none" selected="selected">Select category</option>
          <?php foreach($categories as $category):?>
          <option value="<?php echo $category->id?>" <?php echo set_select('category', $category->id, False); ?>>
          <?php echo $category->category_name?></option>
          <?php endforeach;?>
          </select>
          <?php echo form_error('category'); ?>
        </div>
      </div>
               <div class="form-group">
        <label  class="col-sm-6 control-label">Covernotes Type</label>
        <div class="col-sm-6">
        <select class="input form-control  input-sm selectpicker" name="covernotestype" id="ctype">
          <option value="none" selected="selected">Select Type</option>
          <?php foreach($covernotestypes as $covernotestype):?>
          <option value="<?php echo $covernotestype->id?>" <?php echo set_select('covernotestype', $covernotestype->id, False); ?>>
          <?php echo $covernotestype->type?></option>
          <?php endforeach;?>
          </select>
          <?php echo form_error('covernotestype'); ?>
        </div>
      </div>
          <div class="form-group">
        <label  class="col-sm-6 control-label">ICN NO</label>
        <div class="col-sm-6">
        <input type="text" class="form-control input input-sm"  name="icn" value="<?php echo set_value('icn')?>" placeholder="ICN NO">
        <?php echo form_error('icn'); ?>
        </div>
      </div>

  
      <div class="form-group">
        <label  class="col-sm-6 control-label">Period</label>
        <div class="col-sm-6">
        <select class="input form-control  input-sm selectpicker" name="period" id="period">
          <option value="none" selected="selected">Select period</option>
          <?php foreach($period as $period):?>
          <option value="<?php echo $period->id?>" <?php echo set_select('period', $period->id, False); ?>>
          <?php echo $period->period?></option>
          <?php endforeach;?>
          </select>
          <?php echo form_error('period'); ?>

        </div>
      </div>

      <div class="form-group">
        <label class="col-sm-6 control-label">Sticker number</label>
        <div class="col-sm-6">
        <input type="text" class="form-control input input-sm" name="stickerno" value="<?php echo set_value('stickerno')?>" placeholder="Sticker NO">
        <?php echo form_error('stickerno'); ?>
        </div>
      </div>
   
      <div class="form-group">
        <label  class="col-sm-6 control-label">Issue date</label>
      
         <div class="col-sm-6">
    <div class='input-group start date'>
            <input type='text'  id ="i_date" name="i_date" class="form-control  input-sm" value="<?php echo set_value('i_date')?>" placeholder="Issue date" readonly/>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            </span>
            <?php echo form_error('i_date'); ?>
        </div>
    </div>
      </div>
           <div class="form-group">
        <label  class="col-sm-6 control-label">Expire date</label>
      
         <div class="col-sm-6">
    <div class='input-group start date'>
            <input type='text' id ="e_date" name="e_date" class="form-control  input-sm" value="<?php echo set_value('e_date')?>" placeholder="Expire date" readonly/>
            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            </span>
            
            <?php echo form_error('e_date'); ?>
        </div>
    </div>
      </div>





        </div>


          <!-- Calculation -->
          <div class="col-md-4">
        
        
         <div class="form-group" id="value">
        <label class="col-sm-5 control-label">Sumed Value</label>
        <div class="col-sm-6">
        <input type="number" class="form-control input input-sm" name="value" value="0" placeholder="Sumed value" id="sumedvalue">
        <?php echo form_error('value'); ?>
        </div>
      </div>

      
     <div class="form-group" id="bp">
        <label class="col-sm-5 control-label">Basic premium</label>
        <div class="col-sm-6">
        <input type="text" class="form-control input input-sm" name="bpremium" value="<?php echo set_value('bpremium')?>" readonly>
        <?php echo form_error('bpremium'); ?>
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-2 control-label">
            <input type="hidden" name="ebb" value="0">
            <input type="checkbox" name="ebb"  value="1" id="s_ebb" >
            <!-- <input type="checkbox"  name="ebb" value="1" class="s_ebb"/> -->
           
        </div>
        <label  class="col-sm-3 control-label">EBB</label>

        <div class="col-sm-6">

        <div class="input-group">
        <div class="input-group-addon">10&#37</div>
                 <input class="form-control" type="text" name="eb"readonly id="ebb">
                      
          </div>
        </div>

        
          </div>
          <div class="form-group">
        <div class="col-sm-2 control-label">
            <input type="hidden" name="unexperienced" value="0"/>
            <input type="checkbox" name="unexperienced" value="1" id="s_unex" > 
        </div>
        <label  class="col-sm-3 control-label">Unexperienced</label>

        <div class="col-sm-6">

        <div class="input-group">
        <div class="input-group-addon">25&#37</div>
               
              <input class="form-control" type="text" name="unexperience"readonly id="unex">
                      
          </div>
        </div>

        
          </div>
          <div class="form-group">
        <div class="col-sm-2 control-label">

                 <input type="hidden" name="underage" value="0"   />
            <input type="checkbox" name="underage" value="1" id="s_uage" > 
        </div>
        <label  class="col-sm-3 control-label">Under age(<24yrs)</label>

        <div class="col-sm-6">

        <div class="input-group">
        <div class="input-group-addon">25&#37</div>
              <input class="form-control" type="text" name="underage" readonly id="uage">
                      
          </div>
        </div>

        
          </div>
      
      
          <div class="form-group">
        <label class="col-sm-5 control-label">Vat</label>
        <div class="col-sm-6">

        <div class="input-group">
        <div class="input-group-addon">18&#37</div>
              <input class="form-control" type="text" name="vat" readonly id="vat">
                      
          </div>
          
        </div>
      
          </div>
         <div class="form-group" id="np">
        <label class="col-sm-5 control-label">Net Premium</label>
        <div class="col-sm-6">
        <input type="text" class="form-control input input-sm" name="mobile" value="<?php echo set_value('netpremium')?>" readonly id="npvalue">
        <?php echo form_error(); ?>
        </div>
      
          </div>
        </div>
</div>
<div class="row">
  <hr/>
  <div class="form-group">
        <div class="col-sm-offset-5 col-sm-2">
            <button type="submit"  style="background:#009FED;color:white; font-size:12px;" class="btn btn-sm btn-block">Save</button>
        </div>
      </div>
</div>
</form>

    </div>
   </div>
   <div id="menu1" class="tab-pane fade">
      <h3>Menu 2</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
   <div id="menu2" class="tab-pane fade">
      <h3>Menu 2</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
   
    <div id="menu3" class="tab-pane fade">
      <h3>Menu 3</h3>
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
  </div>  




  


    </div>
  </div>
</section>