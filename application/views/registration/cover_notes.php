<section>
<div class="col-md-12">
	<div class="content-box-large">
		<div class="panel-heading">
			<div class="panel-tit6"><h3  align="center">Please Issue cover note</h3>
				<div style="border-bottom: solid black 2px;"></div>
			</div>
		</div>
		<div class="panel-body">
		<form class="form-horizontal" role="form"action="<?php echo base_url("addcovernotes") ?>" method="post">
			
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-6">
					<?php echo $this->session->flashdata('message'); ?>
				</div>
			
			</div>
			<div class="form-group">
				<label  class="col-sm-4 control-label">Cover #</label>
				<div class="col-sm-5">
					<input type="text" class="input form-control input-sm" name="cover_no" value="<?php echo set_value('cover_no')?>" placeholder="Cover number">
					<?php echo form_error('cover_no'); ?>
				</div>
			</div>
		
			<div class="form-group">
				<label  class="col-sm-4 control-label">Cover Notes Type</label>
				<div class="col-sm-5">
					<select class="input form-control  input-sm selectpicker" name="covertype_id">
					<option value="none" selected="selected">Select Type</option>
					<?php foreach($covernotestypes as $covernotestype):?>
					<option value="<?php echo $covernotestype->id?>" <?php echo set_select('covertype_id', $covernotestype->id, False); ?>>
					<?php echo $covernotestype->type?></option>
					<?php endforeach;?>
					</select>
					<?php echo form_error('covertype_id'); ?>
				</div>
			</div>

			<div class="form-group">
				<label  class="col-sm-4 control-label">Insuarer</label>
				<div class="col-sm-5">
					<select class="input form-control input-sm selectpicker" name="beneficiary_id" >
						<option value="none" selected="selected">Select Insuarer</option>
						<?php foreach($beneficiaries as $beneficiary):?>
						<option value="<?php echo $beneficiary->id?>" 
							<?php echo set_select('beneficiary_id', $beneficiary->id, False); ?>>
							<?php echo $beneficiary->name?>
						</option>
						<?php endforeach;?>
					</select>
					<?php echo form_error('beneficiary_id'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword4" class="col-sm-4 control-label">Customer First Name</label>
				<div class="col-sm-5">
					<input type="text" class="input form-control input-sm" name="f_name" value="<?php echo set_value('f_name')?>" placeholder="First Name">
					<?php echo form_error('f_name'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword4" class="col-sm-4 control-label">Customer Last Name</label>
				<div class="col-sm-5">
					<input type="text" class="input form-control input-sm" name="l_name" value="<?php echo set_value('l_name')?>" placeholder="Last Name">
					<?php echo form_error('l_name'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword4" class="col-sm-4 control-label">Customer Email</label>
				<div class="col-sm-5">
					<input type="text" class="input form-control input-sm" name="email" value="<?php echo set_value('email')?>" placeholder="email">
					<?php echo form_error('email'); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label">Vehicle Name</label>
				<div class="col-sm-5">
					<input type="text" class="input form-control input-sm" name="vehicle_name" value="<?php echo set_value('vehicle_name')?>" placeholder="Vehicle name">
					<?php echo form_error('vehicle_name'); ?>
				</div>
			</div>
			<div class="form-group">
				<label  class="col-sm-4 control-label">Vehicle Registration Number</label>
				<div class="col-sm-5">
					<input type="text" class="input form-control input-sm" name="vehicle_no" value="<?php echo set_value('vehicle_no')?>" placeholder="Vehicle number">
					<?php echo form_error('vehicle_no'); ?>
				</div>
			</div>
			<div class="form-group">
				<label  class="col-sm-4 control-label">Customer Mobile #</label>
				<div class="col-sm-5">
					<input type="text" class="input form-control input-sm" name="mobile_no" value="<?php echo set_value('mobile_no')?>" placeholder="moble number">
					<?php echo form_error('mobile_no'); ?>
				</div>
			</div>

			<!-- <div class="form-group">
    			<label  class="col-sm-4 control-label">First date</label>
    			<div class="col-sm-5">
    			<div class='input-group start date'>
            		<input type='text' name="enddate" class="input form-control" name="first_date" value="<?php echo set_value('')?>" placeholder="Last date of the news"/>
            	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            	</span>
        		</div>
    			</div>
  			</div>
  			<div class="form-group">
    			<label  class="col-sm-4 control-label">Expire date</label>
    			<div class="col-sm-5">
    			<div class='input-group start date'>
            		<input type='text' name="enddate" class="input form-control" name="expire_date" value="<?php echo set_value('')?>" placeholder="Last date of the news"/>
            		
            	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            	</span>
        		</div>
    			</div>
  			</div>	 -->

			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-5">
						<button type="submit"  style="background:#ff7f2a;color:white; font-size:12px;" class="btn btn-sm btn-block">Save</button>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-6">
						<button type="submit"  style="background:#ff7f2a;color:white; font-size:12px;" class="btn btn-sm btn-block">Import Covernotes from excel sheet</button>
				</div>
			</div>
		</form>
		</div>
	</div>
</div>
</section>
