	<form class="form-horizontal" role="form"action="<?php echo base_url("setting_classes?c=add") ?>" method="post">
			
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-6">
					<?php echo $this->session->flashdata('message'); ?>
				</div>
			
			</div>
			<div class="form-group">
				<label  class="col-sm-4 control-label">Insurance Class Name</label>
				<div class="col-sm-6">
				<input type="text" class="form-control input input-sm"  name="class" value="<?php echo set_value('class')?>" placeholder="insurance class">
				<?php echo form_error('class'); ?>
				</div>
			</div>
			<div class="form-group">
				<label  class="col-sm-4 control-label">Brokerage</label>
				<div class="col-sm-6">
				<input type="number" size="2" min="0" max="50"class="form-control input input-sm"  name="brokerage" value="<?php echo set_value('brokerage')?>" placeholder="%">
				<?php echo form_error('brokerage'); ?>
				</div>
			</div>						
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-6">
						<button type="submit"  style="background:#ff7f2a;color:white; font-size:12px;" class="btn btn-sm btn-block">Save</button>
				</div>
			</div>
		</form>
