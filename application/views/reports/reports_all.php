

	<div class="content-box-large">
				<div class="panel-heading">
			<div class="panel-tit6"><h4  align="center">List of cover notes</h4>
				<div style="border-bottom: solid black 2px;"></div>
			</div>
		</div>
  				<div class="panel-body">
  					
  				<form class="form-horizontal" role="form">
  				<div class="row">
  				<div class="form-group">
    			<label  class="col-sm-2 control-label">From</label>
    			<div class="col-sm-3">
    			<div class='input-group start date'>
            		<input type='text' name="enddate" class="input form-control" name="first_date" value="<?php echo set_value('')?>" placeholder="From"/>
            	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            	</span>
        		</div>
    			</div>
    			<label  class="col-sm-2 control-label">To</label>
    			<div class="col-sm-3">
    			<div class='input-group start date'>
            		<input type='text' name="enddate" class="input form-control" name="first_date" value="<?php echo set_value('')?>" placeholder="To"/>
            	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            	</span>
        		</div>
    			</div>
    			</div>
    			<div class="row">
    				<div class="col-sm-offset-1 col-sm-10">
    					<div style="border-bottom: solid gray 1px;margin-bottom:10px; "></div>
    				</div>
    			</div>
  			</div>

  				<div class="form-group">
  				<div class="col-sm-1">
					
				</div>
				<div class="col-sm-1">
					<input type="checkbox" class="input checkbox">
				</div>
				<label  class="col-sm-3 control-label">Cover Notes Type</label>
				<div class="col-sm-4">
					<select class="input form-control  input-sm selectpicker" name="covertype_id">
					<option value="none" selected="selected">Select Type</option>
					<?php foreach($covernotestypes as $covernotestype):?>
					<option value="<?php echo $covernotestype->id?>" <?php echo set_select('covertype_id', $covernotestype->id, False); ?>>
					<?php echo $covernotestype->type?></option>
					<?php endforeach;?>
					</select>

					<?php echo form_error('covertype_id'); ?>
				</div>
				<div class="col-sm-3">
					
				</div>

			</div>

			<div class="form-group">
				<div class="col-sm-1">
					
				</div>
				<div class="col-sm-1">
					<input type="checkbox" class="input checkbox">
				</div>

				<label  class="col-sm-3 control-label">Insuarer</label>
				<div class="col-sm-4">
					<select class="input form-control input-sm selectpicker" name="beneficiary_id" >
						<option value="none" selected="selected">Select Insuarer</option>
						<?php foreach($beneficiaries as $beneficiary):?>
						<option value="<?php echo $beneficiary->id?>" 
							<?php echo set_select('beneficiary_id', $beneficiary->id, False); ?>>
							<?php echo $beneficiary->name?>
						</option>
						<?php endforeach;?>
					</select>
					<?php echo form_error('beneficiary_id'); ?>
				</div>

				<div class="col-sm-2">
					
				</div>
			</div>
			 <div class="form-group">
				<label class="col-sm-8 control-label">Total</label>
				<div class="col-sm-1">
				 
						<span class="input-sm input form-control"> </span>
						
				</div>
				<div class="col-sm-3">
					
				</div>
								  </div>
								  <div class="form-group">
				<div class="col-sm-4">
						<a style="background:#2c3742;color:white; font-size:12px;" class="btn btn-sm btn-block" href="<?php echo base_url('')?>">Preview</a>
						
				</div>
				<div class="col-sm-4">
						<a style="background:#2c3742;color:white; font-size:12px;" class="btn btn-sm btn-block" href="<?php echo base_url('')?>">Print</a>
						
				</div>
				<div class="col-sm-4">
						<a style="background:#2c3742;color:white; font-size:12px;" class="btn btn-sm btn-block" href="<?php echo base_url('reports/allCovernotes')?>">Export Excel</a>
						
				</div>
			</div>

			<div style="border-bottom: solid black 2px; margin-bottom:10px;"></div>
  					</form>
  					<div class="table-responsive">
  						<table class="table">
			              <thead>
			                <tr >
			                  <th>#</th>
			                  <!-- <th>Cover #</th> -->
			                  <th>Cover type</th>
			                  <th>Insuarer</th>
			                  <th>First Name</th>
			                  <th>Last Name</th>
			                  <th>Mobile</th>
			                  <!-- <th>Email</th> -->
			                  <th>Vehicle Name</th>
			                  <th>Vehicle Reg #</th>
			                  <!-- <th>First Date</th> -->
			                  <th>Expire Date</th>
			                </tr>
			              </thead>
			              <tbody>
			              <?php foreach ($covernotes as $covernote) {
			              	?>
	
			                <tr class="success">
			                  <td><?php  echo $covernote->id;?></td>
			                  <!-- <td><?php  echo $covernote->cover_no;?></td> -->
			                  <td><?php  echo $covernote->cover_type;?></td>
			                  <td><?php  echo $covernote->beneficiary;?></td>
			                  <td><?php  echo $covernote->f_name;?></td>
			                  <td><?php  echo $covernote->l_name;?></td>
			                  <td><?php  echo $covernote->mobile;?></td>
			                  <!-- <td><?php  echo $covernote->email;?></td> -->
			                  <td><?php  echo $covernote->v_name;?></td>
			                  <td><?php  echo $covernote->v_no;?></td>
			                  <!-- <td><?php  echo $covernote->f_date;?> </td> -->
			                  <td><?php  echo $covernote->e_date;?><td>
			                </tr>
			               
			               <?php } ?>
			              </tbody>
			            </table>
  					</div>
  				</div>
  			</div>
