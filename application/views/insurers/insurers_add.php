	<form class="form-horizontal" role="form"action="<?php echo base_url("insurers?i=add") ?>" method="post">
			
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-6">
					<?php echo $this->session->flashdata('message'); ?>
				</div>
			
			</div>
			<div class="form-group">
				<label  class="col-sm-4 control-label">Insuarer Name</label>
				<div class="col-sm-6">
				<input type="text" class="form-control input input-sm"  name="insurer" value="<?php echo set_value('insurer')?>" placeholder="Insurer's name">
				<?php echo form_error('insurer'); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4 control-label">Email</label>
				<div class="col-sm-6">
				<input type="email" class="form-control input input-sm" name="email" value="<?php echo set_value('email')?>" placeholder="Email">
				<?php echo form_error('email'); ?>
				</div>
			</div>
			<div class="form-group">
				<label for="inputPassword4" class="col-sm-4 control-label">Mobile number</label>
				<div class="col-sm-6">
				<input type="text" class="form-control input input-sm" name="mobile" value="<?php echo set_value('mobile')?>" placeholder="+255XXXXXXXXX">
				<?php echo form_error('mobile'); ?>
				</div>
			</div>						
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-6">
						<button type="submit"  style="background:#ff7f2a;color:white; font-size:12px;" class="btn btn-sm btn-block">Save</button>
				</div>
			</div>
		</form>
