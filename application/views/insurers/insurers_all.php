<section>
<div class="col-md-12">
	<div class="content-box-large">
		<div class="panel-heading">
			<div class="panel-tit6"><h3  align="center">Insurers panel</h3>
				<div style="border-bottom: solid black 2px;"></div>
			</div>
		</div>
		<div class="panel-body">
		<div class="raw">	
		
</div>	
<div class="row">
	<div class="col-md-10">
			<?php $this->load->view($insurers); ?>
	</div>
	<div class="col-md-2">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<a class="btn-sm btn btn-default btn-block" href="<?php echo base_url('insurers/insurers?i=add'); ?>">add</a>
				</div>
				<div class="form-group">
					<a class="btn-sm btn btn-default btn-block" href="<?php echo base_url('insurers/insurers?i=view'); ?>">view</a>
				</div>
				
			</div>
		</div>
	</div>
</div>

	

		</div>
	</div>
</div>
</section>
