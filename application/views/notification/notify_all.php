<div class="content-box-large">
				<div class="panel-heading">
			<div class="panel-tit6"><h4  align="center">Notification to beneficiaries</h4>
				<div style="border-bottom: solid black 2px;"></div>
			</div>
		</div>
  				<div class="panel-body">
  					
  				<form class="form-horizontal" role="form" method="post" action="<?php echo base_url('notification/beneficiary')?>">
  				<div class="row">
  				<div class="form-group">
				<div class="col-sm-offset-3 col-sm-6">
					<?php echo $this->session->flashdata('message'); ?>
				</div>
			
			</div>
  				<div class="form-group">
    			<label  class="col-sm-2 control-label">From</label>
    			<div class="col-sm-3">
    			<div class='input-group start date'>
            		<input type='text' name="enddate" class="input form-control" name="first_date" value="<?php echo set_value('')?>" placeholder="From"/>
            	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            	</span>
        		</div>
    			</div>
    			<label  class="col-sm-2 control-label">To</label>
    			<div class="col-sm-3">
    			<div class='input-group start date'>
            		<input type='text' name="enddate" class="input form-control" name="first_date" value="<?php echo set_value('')?>" placeholder="To"/>
            	<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
            	</span>
        		</div>
    			</div>
    			</div>
    			<div class="row">
    				<div class="col-sm-offset-1 col-sm-10">
    					<div style="border-bottom: solid gray 1px;margin-bottom:10px; "></div>
    				</div>
    			</div>
  			</div>

  				<div class="form-group">
  				<div class="col-sm-1">
					
				</div>
				<div class="col-sm-1">
					<input type="checkbox" class="input checkbox">
				</div>
				<label  class="col-sm-3 control-label">Cover Notes Type</label>
				<div class="col-sm-4">
					<select class="input form-control  input-sm selectpicker" name="covertype_id">
					<option value="none" selected="selected">Select Type</option>
					<?php foreach($covernotestypes as $covernotestype):?>
					<option value="<?php echo $covernotestype->id?>" <?php echo set_select('covertype_id', $covernotestype->id, False); ?>>
					<?php echo $covernotestype->type?></option>
					<?php endforeach;?>
					</select>

					<?php echo form_error('covertype_id'); ?>
				</div>
				<div class="col-sm-3">
					
				</div>

			</div>

			<div class="form-group">
				<div class="col-sm-1">
					
				</div>
				<div class="col-sm-1">
					<input type="checkbox" class="input checkbox">
				</div>

				<label  class="col-sm-3 control-label">Insuarer</label>
				<div class="col-sm-4">
					<select class="input form-control input-sm selectpicker" name="beneficiary_id[]" >
						<option value="none" selected="selected">Select Insuarer</option>
						<?php foreach($beneficiaries as $beneficiary):?>
						<option value="<?php echo $beneficiary->id?>,
										<?php echo $beneficiary->name?>,
										<?php echo $beneficiary->email?>" 
							<?php echo set_select('beneficiary_id', $beneficiary->id, False); ?>>
							<?php echo $beneficiary->name?>
						</option>
						<?php endforeach;?>
					</select>
					<?php echo form_error('beneficiary_id'); ?>
				</div>

				<div class="col-sm-2">
					
				</div>
			</div>
			 <div class="form-group">
				<label class="col-sm-8 control-label">Total</label>
				<div class="col-sm-1">
				 
						<span class="input-sm input form-control"> </span>
						
				</div>
				<div class="col-sm-3">
					
				</div>
								  </div>
								  <div class="form-group">
				<div class="col-sm-4">
						<a style="background:#2c3742;color:white; font-size:12px;" class="btn btn-sm btn-block" href="<?php echo base_url('')?>">Preview</a>
						
				</div>
				<div class="col-sm-4">
						<a style="background:#2c3742;color:white; font-size:12px;" class="btn btn-sm btn-block" href="<?php echo base_url('')?>">Print</a>
						
				</div>
				<div class="col-sm-4">
						<button type="submit"  style="background:#ff7f2a;color:white; font-size:12px;" class="btn btn-sm btn-block">Notify</button>
				</div>
			</div>


			<div style="border-bottom: solid black 2px; margin-bottom:10px;"></div>
  					</form>
  			
  				</div>
  			</div>
