<form class="form-horizontal" role="form"action="<?php echo base_url("class_motors?v=add") ?>" method="post">
			
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-6">
					<?php echo $this->session->flashdata('message'); ?>
				</div>
			
			</div>
			<div class="form-group">
				<label  class="col-sm-4 control-label">Motor vehicle insurance category</label>
				<div class="col-sm-6">
				<input type="text" class="form-control input input-sm"  name="category" value="<?php echo set_value('category')?>" placeholder="Motor vehicle insurance category">
				<?php echo form_error('category'); ?>
				</div>
			</div>					
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-6">
						<button type="submit"  style="background:#ff7f2a;color:white; font-size:12px;" class="btn btn-sm btn-block">Save</button>
				</div>
			</div>
		</form>
