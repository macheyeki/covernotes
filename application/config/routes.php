<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'users';

$route['covernotes_types'] = 'covernotes/coverNotestypes';



$route['addcovernotes'] = 'covernotes/coverNotes';
$route['addcovernotetype'] = 'registration/coverNotestypes';
$route['statistics'] = 'statistics/statistics';




$route['mailcustomer'] = 'mail/sendEmail';
$route['mailinsuarer'] = 'notification/sendEmail';
$route['404_override'] = '';

// inuse
$route['insurance'] = 'covernotes/covernotes';
$route['addmotors'] = 'covernotes/motorsAdd';
$route['motorsformular'] = 'covernotes/getMotorsformular';


$route['insurers'] = 'insurers/insurers';
$route['covers_reports'] = 'reports/coversReports';
$route['coverage_reports'] = 'reports/coverageReports';

$route['class_motors'] = 'classes/motorVehicles';
$route['contractors'] = 'classes/contractors';

$route['setting_covernotes'] = 'settings/covernotesDefinition';
$route['setting_classes'] = 'settings/classDefinition';
$route['classenable'] = 'settings/classEnable';
$route['setting_notification'] = 'settings/notifications';

// reports
$route['report_one'] = 'reports/reportOne';
$route['report_two'] = 'reports/reportTwo';
$route['report_three'] = 'reports/reportThree';
$route['report_four'] = 'reports/reportFour';
// end reports

$route['translate_uri_dashes'] = FALSE;
